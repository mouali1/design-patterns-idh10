#!/bin/bash
#
# Args: deploy.sh
#

cd ~

wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.3.0.1492-linux.zip

unzip sonar-scanner-cli-3.3.0.1492-linux.zip

rm sonar-scanner-cli-3.3.0.1492-linux.zip

chmod 777 sonar-scanner-3.3.0.1492-linux/conf/sonar-scanner.properties

echo 'sonar.host.url=http://test.nl' >> sonar-scanner-3.3.0.1492-linux/conf/sonar-scanner.properties

chmod +x sonar-scanner-3.3.0.1492-linux/bin/sonar-scanner

sonar-scanner-3.3.0.1492-linux/bin/sonar-scanner \
  -Dsonar.projectKey=test \
  -Dsonar.sources=. \
  -Dsonar.host.url=test.nl \